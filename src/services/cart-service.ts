import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {Http,Headers,Response,RequestOptions} from "@angular/http";
import {Environment} from "../environment/env";
const STORAGE_KEY = 'cartItems';
const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};
@Injectable()
export class CartService {
    
    constructor(public http: Http,public storage: Storage) { 
       
    }
    getCartItems(){
        return this.getAll();
    }
    addToCart(item){
       debugger; 
       return  this.getAll().then(cartitems=>{
            if (cartitems) {
                var flag = true;
                cartitems.forEach(function(element,i){
                    if(element.product_id==item.product_id){
                        cartitems[i].quantity = item.quantity;
                        flag = false;
                    }
                })
                if(flag){
                cartitems.push(item);
                }
                return this.storage.set(STORAGE_KEY, cartitems);
            } else {
                return this.storage.set(STORAGE_KEY, [item]);
            }

        })
    }
    removeFromCart(item){
        return  this.getAll().then(cartitems=>{
            if (cartitems) {
                cartitems.forEach(function(element,i){
                    if(element.product_id==item){
                        var deletedItem = cartitems.splice(i,1);
                    }
                })
                return this.storage.set(STORAGE_KEY, cartitems);
            } else {
                return this.storage.set(STORAGE_KEY, []);
            }

        })
    }
   updateCart(item,quantity){
        return  this.getAll().then(cartitems=>{
            if (cartitems) {
                
                for(var i=0;i<cartitems.length;i++){
                    if(cartitems[i].product_id==item){
                        cartitems[i].quantity = quantity;
                    }
                }
                return this.storage.set(STORAGE_KEY, cartitems);
            } else {
                return this.storage.set(STORAGE_KEY, []);
            }

        })
    }
    
    placeOrder(payload){
        return this.http.post(Environment.URL+"api/order/create",payload,httpOptions);
    }
    emptyCart(){
        return this.storage.set(STORAGE_KEY, []);
    }
     getAll(){
        return this.storage.get(STORAGE_KEY);
    }
    getCustomer(){
        return this.storage.get("user");    
    }


}