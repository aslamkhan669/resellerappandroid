import {Injectable} from "@angular/core";
import {Observable} from "rxjs/observable";
import {Http,Headers,Response,RequestOptions} from "@angular/http";
import {Environment} from "../environment/env";
import { Storage } from '@ionic/storage';
 
const STORAGE_KEY = 'user';

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()

export class AuthenticationService {
  private http:any;
  
  constructor(http: Http,public storage: Storage){
    this.http = http;
  }

  signin(phone:string) {
    var body = {phone:phone};  
    return this.http.post(Environment.URL+"api/customer/authenticate",body);
      

  }

  setUser(user){
       return this.storage.set(STORAGE_KEY, user);
  }
   getCustomer(){
        return this.storage.get("user");    
    }

  
}
