import {Injectable} from "@angular/core";
import {Observable} from "rxjs/observable";
import {Http,Headers,Response,RequestOptions} from "@angular/http";
import {Environment} from "../environment/env";
import { Storage } from '@ionic/storage';
 
const STORAGE_KEY = 'user';

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()

export class OrderService {
  private http:any;
  
  constructor(http: Http,public storage: Storage){
    this.http = http;
  }

  myorders(phone:string) {
    var payload = {phone:phone};  
    return this.http.post(Environment.URL+"api/order/myorders",payload,httpOptions);
      

  }
  getItems(id){
    return this.http.get(Environment.URL+"api/order/orderitems?id="+id,httpOptions);
  }
  setUser(user){
       return this.storage.set(STORAGE_KEY, user);
  }
  getUser(){
       return this.storage.get(STORAGE_KEY);
  }

  
}
