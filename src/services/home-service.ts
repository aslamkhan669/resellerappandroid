import {Injectable} from "@angular/core";
import {Observable} from "rxjs/observable";
import {Http,Headers,Response,RequestOptions} from "@angular/http";
import {Environment} from "../environment/env";

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()

export class HomeService {
  private http:any;
  
  constructor(http: Http){
    this.http = http;
  }

  getallCategories() {
     
    return this.http.get(Environment.URL+"api/product/categories");

  }
  getBanners(){
    return this.http.get(Environment.URL+"api/banners");
  }
  

  getallCategoryImages(id) {
     var payload = {catid:id}
    return this.http.post(Environment.URL+"api/product/imagesbycategories",payload,httpOptions);

  }
  
  
}
