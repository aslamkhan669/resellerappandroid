import {Injectable} from "@angular/core";
import {Observable} from "rxjs/observable";
import {Environment} from "../environment/env";
import {Http,Headers,Response,RequestOptions} from "@angular/http";


const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()

export class ProductService {
  private http:any;
  
  constructor(http: Http){
    this.http = http;
  }

  getallproducts(categoryId:Number,page) {
     
    return this.http.get(Environment.URL+"api/product/category?id="+categoryId+"&page="+page);
    

  }
  getcategoryDescription(categoryId){
    
    return this.http.get(Environment.URL+"api/product/getcategorybyid?id="+categoryId);
  }
  searchProduct(keyword){
    return this.http.get(Environment.URL+"api/product/search?keyword="+keyword);
  }
  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
  
  getallCategoryImages(id) {
    var payload = {catid:id}
   return this.http.post(Environment.URL+"api/product/imagesbycategories",payload,httpOptions);

 }

  
}
