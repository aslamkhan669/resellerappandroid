import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { MyorderPage } from "../pages/myorder/myorder";
import { OrderPage } from "../pages/order/order";
import {AuthenticationService} from "../services/authenticate-service";
import {ImgCacheService} from 'ng-imgcache'

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  phone:string = "";
  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    public user : AuthenticationService,
    public imgCache : ImgCacheService
  ) {
    this.initializeApp();
    this.user.getCustomer().then(cust=>{
      this.phone = cust.phone;
    }).catch(function(error){})
    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Cart', component: MyorderPage, icon: 'cart'},
      {title: 'My Order', component: OrderPage, icon: 'basket'},
      {title: 'Log Out', component: LoginPage, icon: 'log-out'}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.imgCache.init({});
      //*** Control Keyboard
      this.keyboard.disableScroll(true);
      
     
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    
    if(page.title == "Log Out"){
      var data = {verified:false};
      this.user.setUser(data).then(u=>{
        
        this.nav.setRoot(LoginPage);
      }).catch(e=>{});
    }else{
      this.nav.push(page.component);
    }

  }

  logout() {
    var data = {verified:false};
    
    this.user.setUser(data).then(u=>{
      
      this.nav.setRoot(LoginPage);
    }).catch(e=>{});
    
  }

}
