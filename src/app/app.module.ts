import {NgModule} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {SharePage} from "../pages/share/share";
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';
import {HttpModule,Headers,Response,RequestOptions} from "@angular/http";
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Clipboard } from '@ionic-native/clipboard';
import {ImgCacheModule} from 'ng-imgcache';
import {Toast} from '@ionic-native/toast'
import {MyApp} from "./app.component";
import {HomePage} from "../pages/home/home";
import {LoginPage} from "../pages/login/login";
import {SuitsPage} from "../pages/suits/suits";
import {MyorderPage} from "../pages/myorder/myorder";
import { OrderPage } from "../pages/order/order";
import { OrderItemPage } from "../pages/orderitems/orderitems";
import { SearchPage } from "../pages/search/search";
import {AuthenticationService} from "../services/authenticate-service";
import {HomeService} from "../services/home-service";
import {ProductService} from "../services/product-service";
import {CartService} from "../services/cart-service";
import {OrderService} from "../services/order-service";
import { IonicImageLoader } from 'ionic-image-loader';



// import services
// end import services
// end import services

// import pages
// end import pages

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MyorderPage,
    SharePage,
    OrderPage,
    SuitsPage,
    OrderItemPage,
    SearchPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ImgCacheModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicImageLoader.forRoot(),
    IonicStorageModule.forRoot({
      name: 'abrish_attire',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
       
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MyorderPage,
    SharePage,
    OrderPage,
    SuitsPage,
    SearchPage,
    OrderItemPage
  ],
  providers: [
    AuthenticationService,
    SocialSharing,
    PhotoViewer,
    Clipboard,
    Toast,
    StatusBar,
    SplashScreen,
    Keyboard,
    HomeService,
    ProductService,
    CartService,
    OrderService
    
  ]
})

export class AppModule {
}
