import {Component} from "@angular/core";
import {LoadingController,NavController,NavParams, PopoverController, AlertController} from "ionic-angular";
import { SocialSharing } from '@ionic-native/social-sharing';
import {SharePage} from "../share/share";
import {MyorderPage} from "../myorder/myorder";
import {OrderService} from "../../services/order-service";
import {CartService} from "../../services/cart-service";
import {Environment} from "../../environment/env";




@Component({
  selector: 'page-orderitems',
  templateUrl: 'orderitems.html'
})
export class OrderItemPage {
  testCheckboxOpen: boolean;
  testCheckboxResult;
  suitlist:any;
  
  public URL:any;
  public cartProducts:Number;
  public items;
  
  constructor(public nav: NavController,public loading: LoadingController,public navParams: NavParams,public socialSharing:SocialSharing,private orderservice:OrderService,public cart:CartService, public popoverCtrl: PopoverController, public alertCtrl: AlertController) {
    this.URL = Environment.URL+"api/uploads/documents";
    
    let loader = this.loading.create({
      content:"Fetching Items..."
    })
    loader.present();
    orderservice.getItems(this.navParams.get('orderid')).subscribe(res=>{
      loader.dismissAll();
      this.items=res.json();
    })
        
  }

  

  
}
