import { Component } from '@angular/core';
import {LoadingController, NavController } from 'ionic-angular';
import {CartService} from "../../services/cart-service";
import {OrderPage} from "../order/order";
import {Environment} from "../../environment/env";
import { Toast } from '@ionic-native/toast';
@Component({
  selector: 'page-local-weather',
  templateUrl: 'myorder.html'
})
export class MyorderPage {
  public suit: any;
  public items = 0;
  public itemIndex = 0;
  public total=0;
  public totalItems = 0;
  public error = "";
  public hasOrder:boolean = false;
  public URL = Environment.URL+"api/uploads/documents";
customTrackBy(index: number, obj: any): any {
  return index;
}



  constructor(public nav: NavController,public toast:Toast,public loading: LoadingController, public cart: CartService) {
   //this.initializeData();
    
  }
  ionViewWillEnter(){
    this.initializeData();
  }
  initializeData(){
    let loader =  this.loading.create({
      content:"Fetching Cart Items"
    });
    loader.present();
      this.cart.getCartItems().then(cartitems=>{
      if(cartitems!=null){
        if(cartitems.length>0){
          this.suit = cartitems;
          this.calculateAmount();
          this.hasOrder =true;
        }
       }
       else{
        this.hasOrder =false;
       }
        loader.dismissAll();
      }).catch(function(error){})
  }
removeItem(id:number){
 
  this.cart.removeFromCart(id).then(cartitems=>{
    this.suit = cartitems;
    if(cartitems.length==0){
      this.hasOrder =false;
    }
    this.calculateAmount();
  }).catch(function(error){})

}

calculateAmount(){
  this.total = 0;
  this.totalItems = 0;
  for(var i=0; i<this.suit.length;i++){
    this.total+=parseFloat(this.suit[i].price)*parseFloat(this.suit[i].quantity);
    this.totalItems+=this.suit[i].quantity;
    this.suit[i].error = "";
  }
}


// minus children when click minus button
 minus(i:number,qty:number,stock){
  
   if(this.suit[i].quantity>1){
    this.suit[i].quantity--;
    
    this.cart.updateCart(this.suit[i].product_id,this.suit[i].quantity).then(cartitems=>{
      this.calculateAmount();
    }).catch(function(error){})
   }
   else{
    this.suit[i].error = "Quantity Cannot be less than 1";
   }
 }

// plus children when click plus button
 plus(i:number,qty:number,stock) {
   
    if(this.suit[i].quantity<stock && stock>0){
      this.suit[i].quantity++;
      
      this.cart.updateCart(this.suit[i].product_id,this.suit[i].quantity).then(cartitems=>{
        this.calculateAmount();
      }).catch(function(error){})
    }
    else{
      this.suit[i].error = "Quantity Cannot be greater than maximum stock available";
    }
 
 }

 checkout(){
  let loader =  this.loading.create({
    content:"Fetching Cart Items"
  });
  loader.present();
   this.cart.getCustomer().then(data=>{
      this.cart.getCartItems().then(cartitems=>{
        
        if(cartitems.length>0){
          var items = [];
          for(var i=0;i<cartitems.length;i++){
            
              items.push({product_id: cartitems[i].product_id,
              product: cartitems[i].product,
              price: cartitems[i].price,
              product_image: cartitems[i].product_image,
              quantity:cartitems[i].quantity,
              sku:cartitems[i].sku
            
          });
        }
        var payload = {order:{
            customer: data.name,
            phone: data.phone,
            email: data.email,
            status: "Placed",
            address: data.address,
            orderitems:items
          }
        };
        this.cart.placeOrder(payload).subscribe(res=>{
            var data = res.json();
            if(data.status){
              this.cart.emptyCart();
              loader.dismissAll();
              var msg  = {message:data.message};
              this.toast.show(`Your order is placed Successfully. We will contact you within 24 hours for payment details. `, '8000', 'center').subscribe(
                toast => {
                  this.nav.setRoot(OrderPage,msg);
                }
              );
              
            }else{
              loader.dismissAll();
            }
        });
      }
      }).catch(function(error){})
    
    

    })
  

 }

}
