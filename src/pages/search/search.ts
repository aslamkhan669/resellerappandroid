import {Component} from "@angular/core";
import {LoadingController,NavController,NavParams, PopoverController, AlertController} from "ionic-angular";
import { SocialSharing } from '@ionic-native/social-sharing';
import {SharePage} from "../share/share";
import {MyorderPage} from "../myorder/myorder";
import {ProductService} from "../../services/product-service";
import {CartService} from "../../services/cart-service";
import {Environment} from "../../environment/env";
import { IonicImageLoader } from 'ionic-image-loader';
import { PhotoViewer } from '@ionic-native/photo-viewer';


@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  testCheckboxOpen: boolean;
  testCheckboxResult;
  suitlist:any;
  private itemtobeadded:any;
  public URL:any;
  public cartProducts:Number;
  public page =1;
  public doscroll = true;
  public errorMessage = "";
  public keyword = "";
  public cartButtonText = [];
  public catid = 0;
  public catDescription = "";
  constructor(public photoViewer:PhotoViewer,public nav: NavController,public loading: LoadingController,public navParams: NavParams,public socialSharing:SocialSharing,private products:ProductService,public cart:CartService, public popoverCtrl: PopoverController, public alertCtrl: AlertController) {
    
    
  }
  ionViewWillEnter(){
    this.initializeData();
  }
  initializeData(){
    this.URL = Environment.URL+"api/uploads/documents";
    this.catid = this.navParams.get('categoryId');
    let loader = this.loading.create({
      content:"Fetching Items..."
    })
    loader.present();
    this.keyword = this.navParams.get("keyword");
    this.products.searchProduct(this.keyword).subscribe(
      res => {
        
        var allsuits =res.json(); 
        this.cart.getAll().then(data=>{   
        for(var i=0;i<allsuits.length;i++){
          if(allsuits[i].stock_quantity == 0){
            allsuits[i].cartButtonText = "Out of Stock"
          }else{
            allsuits[i].cartButtonText = "Add To Cart"
          }
          if(allsuits[i].product_image == null){
            allsuits[i].product_image = "default.png";
          }
          if(data!=null){
            for(var j=0;j<data.length;j++){
                if(allsuits[i].id == data[j].product_id){
                  allsuits[i].cartButtonText = "Item Added In cart"
                }
            }
          }
        }
        this.suitlist = allsuits;
         
        this.cartProducts = data.length;
        }).catch(function(error){})
        loader.dismissAll();
      });
  }
  presentShare(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(SharePage);
    popover.present({
      ev: myEvent
    });
  }
  myorder(){
    this.nav.push(MyorderPage);
  }
  copyContent(text){
    
  }
  
  whatsappShareAll(id){
    let loader = this.loading.create({
      content:"Preparing list to share..."
    })
    loader.present();
    this.products.getallCategoryImages(id).subscribe(res=>{
      var images = [];

      var URL = Environment.URL+"api/uploads/documents";
     
      if(res){
        
        var data = res.json();
        for(var i=0;i<data.length;i++){
          images.push(URL+data[i].product_image);
          
        }
        this.socialSharing.shareViaWhatsApp(null,images,null).then(() => {
          loader.dismiss();
        }).catch((e) => {
          
        });
      }

    })
   
}
  whatsappShare(index){
    let loader = this.loading.create({
      content:"Fetching Items..."
    })
    loader.present();
    var msg  = [ Environment.URL +"api/uploads/documents"+this.suitlist[index].product_image];

    
    this.socialSharing.shareViaWhatsApp(null,msg,null).then(() => {
      loader.dismiss();
    }).catch((e) => {
      
    });
   }
   facebookShare(index){
    let loader = this.loading.create({
      content:"Fetching Items..."
    })
    loader.present();
    var msg  = [Environment.URL+"api/uploads/documents"+this.suitlist[index].product_image];

    
    this.socialSharing.shareViaFacebook(null,msg,null).then(() => {
      loader.dismiss();
    }).catch((e) => {
      
    });
   }
 


  buyproduct(index,id,name,price,sku,image,stock){
   
    this.itemtobeadded = {
      product_id: id,
      product: name,
      price: price,
      product_image: image,
      stock_quantity:stock,
      sku:sku,
      quantity:1
    }
    if(this.itemtobeadded.quantity<=stock && stock!=0){
      
      this.cart.addToCart(this.itemtobeadded).then(items=>{
        this.cartProducts = items.length;
        this.suitlist[index].cartButtonText = "Item Added";
      })
      
    }
    
  
  }
  showImage(img){
    
    this.photoViewer.show(this.URL+img);
  }
 search(e,keyword){
   if(e==13){
    let loader = this.loading.create({
      content:"Fetching Items..."
    })
    loader.present();
    this.products.searchProduct(keyword).subscribe(
      res => {
        var allsuits =res.json(); 
        this.cart.getAll().then(data=>{   
        for(var i=0;i<allsuits.length;i++){
          if(allsuits[i].stock_quantity == 0){
            allsuits[i].cartButtonText = "Out of Stock"
          }else{
            allsuits[i].cartButtonText = "Add To Cart"
          }
          if(allsuits[i].product_image == null){
            allsuits[i].product_image = "default.png";
          }
          if(data!=null){
            for(var j=0;j<data.length;j++){
                if(allsuits[i].id == data[j].product_id){
                  allsuits[i].cartButtonText = "Item Added In cart"
                }
            }
          }
        }
        this.suitlist = allsuits;
         
        this.cartProducts = data.length;
        }).catch(function(error){})
        loader.dismiss();
      });
   }
  
 }
 
  

  

  
}
