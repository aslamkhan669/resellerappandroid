import {Component} from "@angular/core";
import {LoadingController,NavController, Loading} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {HomeService} from "../../services/home-service";
import {SuitsPage} from "../suits/suits";
import {MyorderPage} from "../myorder/myorder";
import {Environment} from "../../environment/env";
import { SocialSharing } from '@ionic-native/social-sharing';
import {CartService} from "../../services/cart-service";
import { SearchPage } from "../search/search";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  public images = [];
  public suits: any;
  public categories:any;
  public URL:string;
  public loader:any;
  public cartProducts = 0;
  public keyword ="";
  constructor(private storage: Storage,public cart:CartService,public loading: LoadingController, public socialSharing:SocialSharing,public nav: NavController,private home:HomeService) {
    
      
  }
  initializeData(){
    this.URL = Environment.URL+"api/uploads/documents/";
    this.home.getBanners().subscribe(res=>{
      var img = res.json();
      for(var k=0;k<img.length;k++){
        this.images.push(this.URL + img[k].image);
      }
    })
    
      this.loader = this.loading.create({
        content:"Please wait..."
      })
      this.loader.present();
       this.home.getallCategories().subscribe(res=>{
        
          this.categories=res.json();    
          this.loader.dismissAll();
     
      });
      this.cart.getAll().then(data=>{
        if(data){
         this.cartProducts = data.length;
        }
      });
  }
  ionViewWillEnter(){
    this.initializeData();
  }

  whatsappShare(id){
    this.loader = this.loading.create({
      content:"Preparing list to share..."
    })
    this.loader.present();
    this.home.getallCategoryImages(id).subscribe(res=>{
      var images = [];

      var URL =Environment.URL+"api/uploads/documents";
     
      if(res){
        
        var data = res.json();
        for(var i=0;i<data.length;i++){
          images.push(URL+data[i].product_image);
          
        }
        this.socialSharing.shareViaWhatsApp(null,images,null).then(() => {
          this.loader.dismissAll();
        }).catch((e) => {
          
        });
      }

    })
   
}
whatsappSendMessage(){
  this.socialSharing.shareViaWhatsAppToReceiver("917599766537","Hello! Can you help me! from android App",null,null).then(()=>{}).catch((e)=>{});
}
search(e,key){
  if( e == 13){
  this.nav.push(SearchPage,{keyword:key});
  }
}
  
view(id:Number){
  
  var data = {categoryId : id};
  this.nav.push(SuitsPage,data);
}
 myorder(){
  this.nav.push(MyorderPage);
 }

}

