import {Component} from "@angular/core";
import {NavController, MenuController} from "ionic-angular";
import {AuthenticationService} from "../../services/authenticate-service";
import {HomePage} from "../home/home";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public phone:string = "";
  public error:string;
  constructor(public nav: NavController, public menu: MenuController,private authenticate:AuthenticationService) {
    this.menu.swipeEnable(false);
    this.authenticate.getCustomer().then(user=>{
      
      if(user.verified){
        this.nav.setRoot(HomePage);
      }
    }).catch(e=>{

    })
  }

  // login and go to home page
  login() {
    if(/^[0-9]+$/.test(this.phone)){
    this.authenticate.signin(this.phone).subscribe(res=>{
        
      var data=res.json();    
      if(data.status){
        this.phone = data.customer.phone;
        this.authenticate.setUser(data.customer);
        this.nav.setRoot(HomePage);
      }
      else{
        this.error = "To register, contact us on 7599766537";
      }
    });
  }
  else{
    this.error = "Please Enter a valid Phone Number!";
  }
    
  
  }

}
