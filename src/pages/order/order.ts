import { Component } from '@angular/core';
import { LoadingController,NavController,NavParams } from 'ionic-angular';

import {OrderService} from "../../services/order-service";
import { OrderItemPage } from '../orderitems/orderitems';

@Component({
  selector: 'page-order',
  templateUrl: 'order.html'
})
export class OrderPage {

public hasOrder:boolean = false;
public orders = [];
public message = "";
  constructor(public navParams:NavParams,public nav: NavController, public loading : LoadingController,public orderservice:OrderService) {
    let loader = this.loading.create({
      content:"Fetching Order History"
    })
    loader.present();
    //this.message = navParams.get("message");
    this.orderservice.getUser().then(user=>{
      
      this.orderservice.myorders(user.phone).subscribe(res=>{
        
        this.orders=res.json(); 
        
        if(this.orders.length>0){
          this.hasOrder = true;
        }
        loader.dismissAll();
      })
    }).catch(function(error){})
 
  }
  details(id){
    var data = {orderid : id};
  this.nav.push(OrderItemPage,data);
  }
  presentModal() {
   
  }



}
